import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';

import { Component, OnInit, OnDestroy } from '@angular/core';

import { environment } from 'src/environments/environment';

type PhotoUrlsGetResponse = {
  success: boolean,
  photoUrls: Array<string>,
  selected: Array<boolean>,
  maxPhotoAmount: number
};

type PhotoSelectResponse = {
  success: boolean
};

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.less']
})
export class AlbumComponent implements OnInit, OnDestroy {
  private routeParams: Subscription;
  albumId: string;
  success: boolean = false;
  photoUrls: Array<string>;
  initSelected: Array<boolean>;
  selectedAmount: number = 0;
  maxPhotoAmount: number;

  constructor(private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    this.routeParams = this.route.params.subscribe(params => {
      this.albumId = params['albumId'];
      this.getPhotoUrls();
    });
  }

  public getPhotoUrls() {
    this.http.post<PhotoUrlsGetResponse>(environment.apiUrl + '/get', JSON.stringify({
      'albumId': this.albumId
    })).subscribe(
      (response: PhotoUrlsGetResponse) => {
        if (response.success) {
          this.success = true;
          this.photoUrls = response.photoUrls.map(photoUrl => environment.backendUrl + photoUrl);
          this.initSelected = response.selected;
          this.selectedAmount = response.selected.filter(b => b).length;
          this.maxPhotoAmount = response.maxPhotoAmount;
        } else {
          alert('Es gab einen Fehler beim Anzeigen des Albums.');
        }
      },
      () => alert('Es gab einen Fehler beim Anzeigen des Albums.')
    );
  }

  public selectPhoto(event: MouseEvent) {
    const element = event.target as HTMLInputElement;
    const wrapperElement = element.parentElement as HTMLDivElement;
    const photoElement = wrapperElement.children.namedItem('photo') as HTMLImageElement;
    const select = !wrapperElement.hasAttribute('selected');
    this.http.post<PhotoSelectResponse>(environment.apiUrl + (select ? '/select' : '/deselect'), JSON.stringify({
      'albumId': this.albumId,
      'filename': photoElement.src.split('/').pop()
    })).subscribe(
      (response: PhotoSelectResponse) => {
        if (response.success) {
          if (select) {
            wrapperElement.setAttribute("selected", "");
            this.selectedAmount++;
          } else {
            wrapperElement.removeAttribute("selected");
            this.selectedAmount--;
          }
        }
      },
      () => alert('Es gab einen Fehler beim Auswählen des Fotos.')
    );
  }

  ngOnDestroy() {
    this.routeParams.unsubscribe();
  }
}
