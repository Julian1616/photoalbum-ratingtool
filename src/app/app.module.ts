import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CreateComponent } from './create/create.component';
import { AlbumComponent } from './album/album.component';

import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { OAuthModule } from 'angular-oauth2-oidc';

const appRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: CreateComponent
  },
  {
    path: ':albumId',
    pathMatch: 'full',
    component: AlbumComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    CreateComponent,
    AlbumComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule,
    HttpClientModule,
    OAuthModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
