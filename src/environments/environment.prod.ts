const backendUrl = 'https://wetter.codes:4300';

export const environment = {
  production: true,
  googleIssuerUrl: 'https://accounts.google.com',
  googleClientId: '200878382006-7jt916cl4k43qe5sb8e1konqo4n8g34d.apps.googleusercontent.com',
  googleScope: 'openid profile email',
  googlePhotosScope: 'https://www.googleapis.com/auth/photoslibrary.readonly',
  backendUrl,
  apiUrl: backendUrl + '/api'
};
